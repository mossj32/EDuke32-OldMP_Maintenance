// "Build Engine & Tools" Copyright (c) 1993-1997 Ken Silverman
// Ken Silverman's official web site: "http://www.advsys.net/ken"
// See the included license file "BUILDLIC.TXT" for license info.
//
// This file has been modified from Ken Silverman's original release
// by Jonathon Fowler (jonof@edgenetwk.com)


#include "mmulti.h"


int myconnectindex, numplayers;
int connecthead, connectpoint2[MAXMULTIPLAYERS];
char syncstate = 0;

int isvalidipaddress(char *st)
{
    return 0;
}

int initmultiplayersparms(int argc, char **argv)
{
    return 0;
}

int initmultiplayerscycle(void)
{
    return 0;
}

void mmulti_initmultiplayers(int argc, char **argv, char damultioption, char dacomrateoption, char dapriority)
{
    numplayers = 1; myconnectindex = 0;
    connecthead = 0; connectpoint2[0] = -1;
}

void mmulti_setpackettimeout(int datimeoutcount, int daresendagaincount)
{
}

void mmulti_uninitmultiplayers(void)
{
}

void mmulti_sendlogon(void)
{
}

void mmulti_sendlogoff(void)
{
}

int mmulti_getoutputcirclesize(void)
{
    return 0;
}

void mmulti_sendpacket(int other, char *bufptr, int messleng)
{
}

int mmulti_getpacket(int *other, char *bufptr)
{
    return 0;
}

void mmulti_flushpackets(void)
{
}

void genericmultifunction(int other, char *bufptr, int messleng, int command)
{
}


