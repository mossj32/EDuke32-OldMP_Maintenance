#ifndef rawinput_h__
#define rawinput_h__

#include "compat.h"

void RI_PollDevices(BOOL loop);
void RI_ProcessMessage(MSG *msg);
BOOL RI_CaptureInput(BOOL grab, HWND target);

uint8_t RI_MouseState( uint8_t Button );

int8_t RI_WheelState();

int32_t RI_CaptureInput(int32_t grab, HWND target);

// [JM] MinGW is being a cunt. Dunno why I have to undef these fucking things
// and redefine GET_RAWINPUT_CODE_WPARAM
#undef VK_LBUTTON
#undef VK_RBUTTON
#undef VK_CANCEL
#undef VK_MBUTTON
#undef GET_RAWINPUT_CODE_WPARAM
#define GET_RAWINPUT_CODE_WPARAM(wParam) ((wParam) & 0xff)

#define VK_LBUTTON        0x01
#define VK_RBUTTON        0x02
#define VK_CANCEL         0x03
#define VK_MBUTTON        0x04    /* NOT contiguous with L & RBUTTON */

// mouse states for RI_MouseState

#define BUTTON_PRESSED    0x01
#define BUTTON_RELEASED   0x02
#define BUTTON_HELD       0x03

#endif // rawinput_h__

