// Windows DIB/DirectDraw interface layer
// for the Build Engine
// by Jonathon Fowler (jonof@edgenetwk.com)

#ifndef __build_interface_layer__
#define __build_interface_layer__ WIN

extern int backgroundidle;	// set to 1 to tell winlayer to go to idle priority when inactive
extern unsigned maxrefreshfreq;

extern int glusecds;

int win_gethwnd(void);
int win_gethinstance(void);

void win_allowtaskswitching(int onf);
int win_checkinstance(void);

extern void (*keypresscallback)(int32_t,int32_t);
extern void (*mousepresscallback)(int32_t,int32_t);
extern void (*joypresscallback)(int32_t,int32_t);

extern void SetKey(int32_t key, int32_t state);

#include "baselayer.h"

#else
#if (__build_interface_layer__ != WIN)
#error "Already using the " __build_interface_layer__ ". Can't now use Windows."
#endif
#endif // __build_interface_layer__

