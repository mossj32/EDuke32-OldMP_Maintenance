// mmulti.h

#ifndef __mmulti_h__
#define __mmulti_h__

#define MAXMULTIPLAYERS 16

extern int myconnectindex, numplayers;
extern int connecthead, connectpoint2[MAXMULTIPLAYERS];
extern char syncstate;
extern int natfree; //Addfaz NatFree

int initmultiplayersparms(int argc, char **argv);
int initmultiplayerscycle(void);

void mmulti_initmultiplayers(int argc, char **argv);
void mmulti_setpackettimeout(int datimeoutcount, int daresendagaincount);
void mmulti_uninitmultiplayers(void);
void mmulti_sendlogon(void);
void mmulti_sendlogoff(void);
int mmulti_getoutputcirclesize(void);
void mmulti_sendpacket(int other, char *bufptr, int messleng);
int mmulti_getpacket(int *other, char *bufptr);
void mmulti_flushpackets(void);
void mmulti_generic(int other, char *bufptr, int messleng, int command);
int isvalidipaddress(char *st);

void nfIncCP(void); //Addfaz NatFree
int nfCheckHF (int other); //Addfaz NatFree
int nfCheckCP(int other); //Addfaz NatFree

#endif	// __mmulti_h__

